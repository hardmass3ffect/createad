import pkg from 'pg';
import inquirer from 'inquirer';
import bcrypt from 'bcrypt';

const { Pool } = pkg;

async function promptConnectionSettings() {
    console.log("Welcome! Fill in the details to connect to the database:");
    const answers = await inquirer.prompt([
        { type: 'input', name: 'user', message: 'Enter database username:' },
        { type: 'input', name: 'host', message: 'Enter database hostname:' },
        { type: 'input', name: 'database', message: 'Enter database name:' },
        { type: 'password', name: 'password', message: 'Enter database password:' },
        {
            type: 'input',
            name: 'port',
            message: 'Enter database port:',
            validate: (input) => !isNaN(input) || 'Please enter a valid port number',
        },
    ]);

    return {
        user: answers.user,
        host: answers.host,
        database: answers.database,
        password: answers.password,
        port: parseInt(answers.port),
    };
}

async function getNextId(connect) {
    const query = `SELECT MAX(id) as max_id FROM "user"`;
    const result = await connect.query(query);
    const maxId = result.rows[0].max_id;

    if (maxId) {
        return maxId + 1;
    } else {
        return 1;
    }
}

async function createUser(connect) {
    console.log("Welcome! Let's create a new administrator.");
    const answers = await inquirer.prompt([
        { type: 'input', name: 'firstName', message: 'Enter first name:' },
        { type: 'input', name: 'lastName', message: 'Enter last name:' },
        { type: 'input', name: 'email', message: 'Enter email:' },
        { type: 'password', name: 'password', message: 'Enter password:' },
    ]);

    const userEntity = {
        firstName: answers.firstName,
        lastName: answers.lastName,
        email: answers.email,
        password: answers.password,
        isAdmin: true,
        is_active: true,
        avatar: 'path_to_avatar.jpg',
    };

    userEntity.id = await getNextId(connect);

    const salt = await bcrypt.genSalt(10);
    userEntity.password = await bcrypt.hash(answers.password, salt);

    const query = `
    INSERT INTO "user"
      (first_name, last_name, email, password, is_admin, is_active, avatar)
    VALUES
      ($1, $2, $3, $4, $5, $6, $7)
    RETURNING *`;
    const values = [
        userEntity.firstName,
        userEntity.lastName,
        userEntity.email,
        userEntity.password,
        userEntity.is_active,
        userEntity.isAdmin,
        'default_avatar.jpg',
    ];

    try {
        const result = await connect.query(query, values);
        const createdUser = result.rows[0];
        console.log('New administrator created successfully:');
        console.log({
            ...createdUser,
            password: '*'.repeat(userEntity.password.length),
        });
    } catch (error) {
        console.error('Error creating administrator:', error);
    }

    askToAddAdmin(connect);
}

async function askToAddAdmin(connect) {
    const additionalAdmin = await inquirer.prompt([
        { type: 'input', name: 'addAdmin', message: 'Add another administrator? (Y/N):' },
    ]);

    if (additionalAdmin.addAdmin.toUpperCase() === 'Y' || additionalAdmin.addAdmin.toUpperCase() === 'YES') {
        createUser(connect);
    } else {
        console.log('Program finished.');
        await connect.end(); // Закрываем подключение к базе данных
    }
}

async function attemptConnection() {
    const connectionSettings = await promptConnectionSettings();
    const connection = new Pool({
        user: connectionSettings.user,
        host: connectionSettings.host,
        database: connectionSettings.database,
        password: connectionSettings.password,
        port: connectionSettings.port,
    });

    try {
        await connection.connect();
        console.log('Connected to the database successfully.');
        await createUser(connection);
    } catch (error) {
        console.error('Error connecting to the database:', error);

        const retryPrompt = await inquirer.prompt([
            { type: 'input', name: 'retry', message: 'Retry connection? (Y/N):' },
        ]);

        if (retryPrompt.retry.toUpperCase() === 'Y' || retryPrompt.retry.toUpperCase() === 'YES') {
            await attemptConnection();
        } else {
            console.log('Program finished.');
        }
    }
}

attemptConnection();